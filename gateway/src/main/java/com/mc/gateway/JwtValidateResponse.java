package com.mc.gateway;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class JwtValidateResponse {

    private String jwtToken;
    private Boolean doseValid;
    private String message;

}
