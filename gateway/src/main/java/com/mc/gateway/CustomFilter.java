package com.mc.gateway;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class CustomFilter extends AbstractGatewayFilterFactory<CustomFilter.Config> {

    private static final String TOKEN_VALIDATION_URL = "http://localhost:8080/api/auth/validate";

    public CustomFilter() {
        super(Config.class);
    }

    @Autowired
    private RouterValidator routerValidator;

    @Autowired
    WebClient.Builder webClientBuilder;

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();
            if (routerValidator.isSecured.test(request)) {
                System.out.println("First pre filter" + exchange.getRequest());
                if (this.isAuthMissing(request)){
                    return this.onError(exchange, "Authorization header is missing in request", HttpStatus.UNAUTHORIZED);
                }
                final String token = this.getAuthHeader(request);

                JwtValidateRequest jwtValidateRequest = JwtValidateRequest.builder()
                        .token(token)
                        .build();
                System.out.println("First pre filter token " + token);

                return webClientBuilder.build()
                        .post()
                        .uri(TOKEN_VALIDATION_URL)
                        .body(Mono.just(jwtValidateRequest), JwtValidateRequest.class)
                        .retrieve()
                        .bodyToMono(JwtValidateResponse.class).flatMap(response -> {
                            if(!response.getDoseValid()){
                                return this.onError(exchange, "Authorization header is missing in request", HttpStatus.UNAUTHORIZED);
                            }
                            return chain.filter(exchange);
                        });
            }

            return chain.filter(exchange).then(Mono.fromRunnable(() -> {
                //System.out.println("First post filter");
            }));
        };
    }

    private Mono<Void> onError(ServerWebExchange exchange, String err, HttpStatus httpStatus) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(httpStatus);
        return response.setComplete();
    }

    private String getAuthHeader(ServerHttpRequest request) {
        String bearerToken = request.getHeaders().getOrEmpty("Authorization").get(0);
        if(!Strings.isNullOrEmpty(bearerToken) && bearerToken.contains("Bearer")){
            bearerToken = bearerToken.substring(7);
            log.info("Get token : {}", bearerToken);
        }
        return bearerToken;
    }

    private boolean isAuthMissing(ServerHttpRequest request) {
        return !request.getHeaders().containsKey("Authorization");
    }

    public static class Config {
        // Put the configuration properties
    }
}
