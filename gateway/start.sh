#!/bin/bash

echo "Building project ..."
mvn clean compile install

name="GATEWAY"
echo "Collect running pid of gateway-0.0.1 application"
id=`echo $(ps aux | grep gateway-0.0.1 | grep -v grep | awk '{print $2}')`

[[ -z "$id" ]] && echo $name "Application is not running" || echo $name "Application PID is = " $id
[[ -z "$id" ]] || echo $name "Application stopping ..."
[[ -z "$id" ]] || kill -9 $id

echo $name "Application starting ..."
nohup java -jar ./target/gateway-0.0.1-SNAPSHOT.jar &
tail -f nohup.out
