# GATEWAY

Spring boot project for microservice with Maven and JDK 8. This application used to store EUREKA information.


## Used Technology

- **Programming language**: Java
- **Framework**: Spring Boot
- **Database**: In-memory (H2 DB)
- **Automated build**: Apache Maven

## Prerequisites

- **JDK 8**
- **Maven**

## How To Run

1. Go to the project root directory.
2. Open terminal in this project root directory.
3. Give executable permission to start.sh, status.sh and stop.sh.
- `chmod a+x start.sh `
- `chmod a+x stop.sh `
- `chmod a+x status.sh `
4. Run script to run application.
- `./start.sh`
5. Project will run in **http://127.0.0.1:8080**
