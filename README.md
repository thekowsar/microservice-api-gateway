
# Microservice + Spring Cloud Gateway + Eureka + JWT-Auth-Server

## Used Technology Stacks:
- **Programming language**: Java
- **Spring boot, Spring Data JPA, Hibernate**
- **Spring Cloud (Spring Cloud Gateway, Eureka)**
- **Auth Server (JWT)**
- **2 Micro Service (HR, Accounts)**
- **Rest Controller**
- **H2DB (Support MySQL and PostgreSQL)**
- **Maven**
- **Angular (Typescript)**
- **Jasper Reports**

## Prerequisites

- **JDK 8**
- **Maven**
- **NPM**


