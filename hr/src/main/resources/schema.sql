drop table if exists SalarySheet;
drop table if exists CompanyBankAccount;
drop table if exists EmployeeBankAccount;
drop table if exists Employee;
drop table if exists Grade;

create table                   Grade
(
gradeOid                       varchar(128)                                             not null,
gradeNumber                    numeric(8,0)                                             not null,
gradeName                      varchar(128),
basic                          numeric(20,6)                                            not null,
houseRentInPer                 numeric(20,6)                                            not null,
medicalAllowanceInPer          numeric(20,6)                                            not null,
createdBy                      varchar(128)                                             not null,
createdOn                      timestamp                                                not null,
updatedBy                      varchar(128),
updatedOn                      timestamp,
constraint                     pk_grade                                                 primary key    (gradeOid)
);

create table                   Employee
(
employeeOid                    varchar(128)                                             not null,
employeeId                     numeric(4,0)                                             not null,
name                           varchar(256)                                             not null,
address                        text,
mobileNo                       varchar(128),
gradeOid                       varchar(128)                                             not null,
createdBy                      varchar(128)                                             not null,
createdOn                      timestamp                                                not null,
updatedBy                      varchar(128),
updatedOn                      timestamp,
constraint                     pk_employee                                              primary key    (employeeOid),
constraint                     uk_employee_id					                        unique         (employeeId),
constraint                     fk_grade_oid_employee                      	            foreign key    (gradeOid) references     Grade(gradeOid)
);

create table                   EmployeeBankAccount
(
bankAccountOid                 varchar(128)                                             not null,
accountName                    varchar(256)                                             not null,
accountType                    varchar(128)                                             not null,
bankAccountNo                  varchar(128)                                             not null,
bankName                       varchar(256)                                             not null,
branchName                     varchar(256)                                             not null,
currentBalance                 numeric(20,6),
employeeOid                    varchar(128)                                             not null,
createdBy                      varchar(128)                                             not null,
createdOn                      timestamp                                                not null,
updatedBy                      varchar(128),
updatedOn                      timestamp,
constraint                     pk_emp_bank_account                                      primary key    (bankAccountOid),
constraint                     uk_bank_account_no					                    unique         (bankAccountNo),
constraint                     fk_employee_oid_bk_ac                     	            foreign key    (employeeOid) references     Employee(employeeOid)
);

create table                   SalarySheet
(
salarySheetOid                 varchar(128)                                             not null,
month                          numeric(2,0)                                             not null,
year                           numeric(4,0)                                             not null,
gradeNumber                    numeric(8,0)                                             not null,
gradeName                      varchar(128),
basic                          numeric(20,6)                                            not null,
houseRentInPer                 numeric(20,6)                                            not null,
houseRent                      numeric(20,6)                                            not null,
medicalAllowanceInPer          numeric(20,6)                                            not null,
medicalAllowance               numeric(20,6)                                            not null,
employeeOid                    varchar(128)                                             not null,
name                           varchar(256)                                             not null,
createdBy                      varchar(128)                                             not null,
createdOn                      timestamp                                                not null,
updatedBy                      varchar(128),
updatedOn                      timestamp,
constraint                     pk_salary_sheet                                          primary key    (salarySheetOid)
);

create table                   CompanyBankAccount
(
bankAccountOid                 varchar(128)                                             not null,
accountName                    varchar(256)                                             not null,
accountType                    varchar(128)                                             not null,
bankAccountNo                  varchar(128)                                             not null,
bankName                       varchar(256)                                             not null,
branchName                     varchar(256)                                             not null,
currentBalance                 numeric(20,6),
createdBy                      varchar(128)                                             not null,
createdOn                      timestamp                                                not null,
updatedBy                      varchar(128),
updatedOn                      timestamp,
constraint                     pk_com_bank_account                                          primary key    (bankAccountOid),
constraint                     uk_com_bank_account_no					                    unique         (bankAccountNo)
);

-- Insert into CompanyBankAccount
INSERT INTO CompanyBankAccount (bankAccountOid, accountName, accountType, bankAccountNo, bankName, branchName, currentBalance, createdBy, createdOn)
VALUES ('1', 'ABC CO', 'Current', '220000123239', 'AB Bank', 'Mohammedpur Branch, Dhaka', 15000000.00, 'System', current_timestamp);

-- Insert into Grade
INSERT INTO Grade (gradeOid, gradeNumber, gradeName, basic, houseRentInPer, medicalAllowanceInPer, createdBy, createdOn)
VALUES ('1', '1', 'Grade One', 35000.00, 20.00, 15.00, 'System', current_timestamp);
INSERT INTO Grade (gradeOid, gradeNumber, gradeName, basic, houseRentInPer, medicalAllowanceInPer, createdBy, createdOn)
VALUES ('2', '2', 'Grade Two', 30000.00, 20.00, 15.00, 'System', current_timestamp);
INSERT INTO Grade (gradeOid, gradeNumber, gradeName, basic, houseRentInPer, medicalAllowanceInPer, createdBy, createdOn)
VALUES ('3', '3', 'Grade Three', 25000.00, 20.00, 15.00, 'System', current_timestamp);
INSERT INTO Grade (gradeOid, gradeNumber, gradeName, basic, houseRentInPer, medicalAllowanceInPer, createdBy, createdOn)
VALUES ('4', '4', 'Grade Four', 20000.00, 20.00, 15.00, 'System', current_timestamp);
INSERT INTO Grade (gradeOid, gradeNumber, gradeName, basic, houseRentInPer, medicalAllowanceInPer, createdBy, createdOn)
VALUES ('5', '5', 'Grade Five', 15000.00, 20.00, 15.00, 'System', current_timestamp);
INSERT INTO Grade (gradeOid, gradeNumber, gradeName, basic, houseRentInPer, medicalAllowanceInPer, createdBy, createdOn)
VALUES ('6', '6', 'Grade Six', 10000.00, 20.00, 15.00, 'System', current_timestamp);



