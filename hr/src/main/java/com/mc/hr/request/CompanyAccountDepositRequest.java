package com.mc.hr.request;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CompanyAccountDepositRequest {

    private Double amount;

    @NotNull(message = "bankAccountNo can not be null")
    @NotEmpty(message = "bankAccountNo can not be empty")
    private String bankAccountNo;

}
