package com.mc.hr.request;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class EmployeeInfoRequest {

    @NotNull(message = "name can not be null")
    @NotEmpty(message = "name can not be empty")
    private String name;

    @NotNull(message = "address can not be null")
    @NotEmpty(message = "address can not be empty")
    private String address;

    @NotNull(message = "mobileNo can not be null")
    @NotEmpty(message = "mobileNo can not be empty")
    private String mobileNo;

    @NotNull(message = "gradeOid can not be null")
    @NotEmpty(message = "gradeOid can not be empty")
    private String gradeOid;

    @NotNull(message = "accountName can not be null")
    @NotEmpty(message = "accountName can not be empty")
    private String accountName;

    @NotNull(message = "accountType can not be null")
    @NotEmpty(message = "accountType can not be empty")
    private String accountType;

    @NotNull(message = "bankAccountNo can not be null")
    @NotEmpty(message = "bankAccountNo can not be empty")
    private String bankAccountNo;

    @NotNull(message = "bankName can not be null")
    @NotEmpty(message = "bankName can not be empty")
    private String bankName;

    @NotNull(message = "branchName can not be null")
    @NotEmpty(message = "branchName can not be empty")
    private String branchName;

}
