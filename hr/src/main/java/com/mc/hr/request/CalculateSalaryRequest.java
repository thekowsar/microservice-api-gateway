package com.mc.hr.request;

import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CalculateSalaryRequest {

    @Min(value = 1, message = "month should not be less than 1")
    @Max(value = 12, message = "month should not be greater than 31")
    @NotNull(message = "month can not be null")
    private Integer month;

    @Min(value = 1000, message = "month should not be less than 1000")
    @Max(value = 9999, message = "month should not be greater than 2000")
    @NotNull(message = "month can not be null")
    private Integer year;

}
