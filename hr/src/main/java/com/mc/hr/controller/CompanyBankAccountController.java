package com.mc.hr.controller;

import com.mc.hr.request.CompanyAccountDepositRequest;
import com.mc.hr.request.LowestGradeRequest;
import com.mc.hr.response.CommonResponse;
import com.mc.hr.response.CompanyAccountResponse;
import com.mc.hr.response.EmployeeListResponse;
import com.mc.hr.service.CompanyBankAccountService;
import com.mc.hr.service.GradeService;
import com.mc.hr.util.ExceptionHandlerUtil;
import com.mc.hr.util.IdGeneratorUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Data
@RestController
@Validated
@Slf4j
@RequestMapping("/v1/company")
public class CompanyBankAccountController {

    @Autowired
    CompanyBankAccountService companyService;

    @Autowired
    IdGeneratorUtil idGeneratorUtil;

    @PutMapping(value = "/account/deposit")
    public ResponseEntity<CommonResponse> depositBalance(
            @RequestHeader(name = "Authorization") String token,
            @Valid @RequestBody CompanyAccountDepositRequest request) throws ExceptionHandlerUtil {
        try{
            log.info("Deposit company account balance : {}", request);
            CommonResponse response = companyService.depositBalance(request, idGeneratorUtil.getUserName(token));
            log.info("Successfully company account deposited : {}", response);
            return new ResponseEntity<CommonResponse>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }

    @GetMapping(value = "/account/get")
    public ResponseEntity<CompanyAccountResponse> get(
            @RequestHeader(name = "Authorization") String token) throws ExceptionHandlerUtil {
        try{
            log.info("Get company account information request: {}", idGeneratorUtil.getUserName(token));
            CompanyAccountResponse response = companyService.getCompanyAccount(idGeneratorUtil.getUserName(token));
            log.info("Successfully company account information get : {}", response);
            return new ResponseEntity<CompanyAccountResponse>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }
}
