package com.mc.hr.controller;

import com.mc.hr.request.LowestGradeRequest;
import com.mc.hr.response.CommonResponse;
import com.mc.hr.response.EmployeeListResponse;
import com.mc.hr.response.GradeListResponse;
import com.mc.hr.service.GradeService;
import com.mc.hr.util.ExceptionHandlerUtil;
import com.mc.hr.util.IdGeneratorUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Data
@RestController
@Validated
@Slf4j
@RequestMapping("/v1/grade")
public class GradeController {

    @Autowired
    GradeService gradeService;

    @Autowired
    IdGeneratorUtil idGeneratorUtil;

    @PutMapping(value = "/basic/lowest-grade")
    public ResponseEntity<CommonResponse> getByOid(
            @RequestHeader(name = "Authorization") String token,
            @Valid @RequestBody LowestGradeRequest request) throws ExceptionHandlerUtil {
        try{
            log.info("Update basic salary by lowest grade : {}", request);
            CommonResponse response = gradeService.updateGradesBasic(request, idGeneratorUtil.getUserName(token));
            log.info("Successfully gateway information get : {}", response);
            return new ResponseEntity<CommonResponse>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }

    @GetMapping(value = "/info/get-all")
    public ResponseEntity<GradeListResponse> get(
            @RequestHeader(name = "Authorization") String token,
            @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
            @RequestParam(name = "limit", required = false, defaultValue = "10") int limit) throws ExceptionHandlerUtil {
        try{
            log.info("Get all grade information request");
            GradeListResponse response = gradeService.getAll(offset, limit, idGeneratorUtil.getUserName(token));
            log.info("Successfully employee grade get : {}", response.getCount());
            return new ResponseEntity<GradeListResponse>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }
}
