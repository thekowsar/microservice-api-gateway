package com.mc.hr.controller;

import com.mc.hr.request.EmployeeInfoRequest;
import com.mc.hr.request.LowestGradeRequest;
import com.mc.hr.response.CommonResponse;
import com.mc.hr.response.EmployeeListResponse;
import com.mc.hr.response.SalarySheetListResponse;
import com.mc.hr.service.EmployeeInfoService;
import com.mc.hr.service.GradeService;
import com.mc.hr.util.ExceptionHandlerUtil;
import com.mc.hr.util.IdGeneratorUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Data
@RestController
@Validated
@Slf4j
@RequestMapping("/v1/employee")
public class EmployeeController {

    @Autowired
    EmployeeInfoService employeeInfoService;

    @Autowired
    IdGeneratorUtil idGeneratorUtil;

    @PostMapping(value = "/info/save")
    public ResponseEntity<CommonResponse> save(
            @RequestHeader(name = "Authorization") String token,
            @Valid @RequestBody EmployeeInfoRequest request) throws ExceptionHandlerUtil {
        try{
            log.info("Employee info save request : {}", request);
            CommonResponse response = employeeInfoService.save(request, idGeneratorUtil.getUserName(token));
            log.info("Successfully employee info saved : {}", response);
            return new ResponseEntity<CommonResponse>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }

    @GetMapping(value = "/info/get-all")
    public ResponseEntity<EmployeeListResponse> get(
            @RequestHeader(name = "Authorization") String token,
            @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
            @RequestParam(name = "limit", required = false, defaultValue = "10") int limit) throws ExceptionHandlerUtil {
        try{
            log.info("Get all employee information request");
            EmployeeListResponse response = employeeInfoService.getAll(offset, limit, idGeneratorUtil.getUserName(token));
            log.info("Successfully employee information get : {}", response.getCount());
            return new ResponseEntity<EmployeeListResponse>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }
}
