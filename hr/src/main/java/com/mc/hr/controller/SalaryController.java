package com.mc.hr.controller;

import com.mc.hr.request.CalculateSalaryRequest;
import com.mc.hr.request.LowestGradeRequest;
import com.mc.hr.response.CommonResponse;
import com.mc.hr.response.SalarySheetListResponse;
import com.mc.hr.service.GradeService;
import com.mc.hr.service.SalaryService;
import com.mc.hr.util.ExceptionHandlerUtil;
import com.mc.hr.util.IdGeneratorUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.security.Principal;

@Data
@RestController
@Validated
@Slf4j
@RequestMapping("/v1/salary")
public class SalaryController {

    @Autowired
    SalaryService salaryService;

    @Autowired
    IdGeneratorUtil idGeneratorUtil;

    @PostMapping(value = "/sheet/calculate")
    public ResponseEntity<CommonResponse> getByOid(
            @RequestHeader(name = "Authorization") String token,
            @Valid @RequestBody CalculateSalaryRequest request) throws ExceptionHandlerUtil {
        try{
            log.info("Calculate salary : {}", request);
            CommonResponse response = salaryService.calculateSalary(request, idGeneratorUtil.getUserName(token));
            log.info("Successfully calculate salary : {}", response);
            return new ResponseEntity<CommonResponse>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }

    @GetMapping(value = "/sheet/get")
    public ResponseEntity<SalarySheetListResponse> get(
            @RequestHeader(name = "Authorization") String token,
            @RequestParam(name = "year", required = true, defaultValue = "2021") int year,
            @RequestParam(name = "month", required = true, defaultValue = "6") int month,
            @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
            @RequestParam(name = "limit", required = false, defaultValue = "10") int limit) throws ExceptionHandlerUtil {
        try{
            log.info("Get all gateway request");
            SalarySheetListResponse response = salaryService.getSalarySheet(month, year, offset, limit, idGeneratorUtil.getUserName(token));
            log.info("Successfully gateway information get : {}", response.getCount());
            return new ResponseEntity<SalarySheetListResponse>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }

    @GetMapping(value = "/sheet/pdf")
    public void getSalarySheetPdf(HttpServletRequest request, HttpServletResponse response,
                                  @RequestHeader(name = "Authorization") String token,
                                  @RequestParam(name = "year", required = true) int year,
                                  @RequestParam(name = "month", required = true) int month
    ) throws Exception {
        try {
            log.info("Request Param for month: {} and year: {}", month, year);
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", String.format("inline; filename=salary-sheet.pdf"));
            salaryService.generatePdfFile(month, year, response.getOutputStream(), idGeneratorUtil.getUserName(token));
        } catch (ExceptionHandlerUtil ex) {
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }
}
