package com.mc.hr.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@Slf4j
@RequestMapping("/v1/test")
public class TestController {

    @GetMapping(value = "/res")
    public ResponseEntity<String> getByOid() throws Exception {
            log.info("Get gateway by oid : ");
            return new ResponseEntity<String>("Test HR Module", HttpStatus.OK);
    }
}
