package com.mc.hr.service;

import com.mc.hr.entity.*;
import com.mc.hr.repository.CompanyBankAccountRepository;
import com.mc.hr.repository.EmployeeBankAccountRepository;
import com.mc.hr.repository.EmployeeRepository;
import com.mc.hr.repository.GradeRepository;
import com.mc.hr.request.CompanyAccountDepositRequest;
import com.mc.hr.request.EmployeeInfoRequest;
import com.mc.hr.response.CommonResponse;
import com.mc.hr.response.EmployeeListResponse;
import com.mc.hr.response.SalarySheetListResponse;
import com.mc.hr.util.ExceptionHandlerUtil;
import com.mc.hr.util.Messages;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

import static com.mc.hr.util.Messages.DATA_NOT_FOUNT;

@Service
@Slf4j
public class EmployeeInfoService {

    private static final Double basicSalaryIncrement = 5000.00;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    GradeRepository gradeRepository;

    @Autowired
    EmployeeBankAccountRepository employeeBankAccountRepository;

    @Transactional
    public CommonResponse save(EmployeeInfoRequest request, String userId) throws ExceptionHandlerUtil {
        String userMessage = Messages.SUCCESSFULLY_EMPLOYEE_INFO_SAVED;
        EmployeeEntity employeeEntity = new EmployeeEntity();
        EmployeeBankAccountEntity employeeBankAccountEntity = new EmployeeBankAccountEntity();
        GradeEntity gradeEntity = gradeRepository.findByGradeOid(request.getGradeOid());
        if(gradeEntity == null){
            throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, String.format("Grade now found by oid : %s", request.getGradeOid()));
        }

        EmployeeBankAccountEntity bankAccountEntity = employeeBankAccountRepository.findByBankAccountNo(request.getBankAccountNo());
        if(bankAccountEntity != null){
            throw new ExceptionHandlerUtil(HttpStatus.FOUND, String.format("Bank account number found by : %s", request.getBankAccountNo()));
        }

        try{
            BeanUtils.copyProperties(request, employeeEntity);
            employeeEntity.setCreatedBy(userId);
            employeeEntity.setCreatedOn(new Timestamp(new Date().getTime()));

            BeanUtils.copyProperties(request, employeeBankAccountEntity);
            employeeBankAccountEntity.setCreatedBy(userId);
            employeeBankAccountEntity.setCreatedOn(new Timestamp(new Date().getTime()));

            employeeEntity.setGrade(gradeEntity);
            employeeEntity.setEmployeeId(getEmpID());
            EmployeeEntity savedEmployee = employeeRepository.save(employeeEntity);

            employeeBankAccountEntity.setEmployee(savedEmployee);
            employeeBankAccountEntity.setCurrentBalance(0.0);
            employeeBankAccountRepository.save(employeeBankAccountEntity);

        } catch (Exception e){
            log.error("Failed to save Employee data {} for {}", request, e.getMessage());
            throw new ExceptionHandlerUtil(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        return CommonResponse.builder()
                .obj("success")
                .userMessage(userMessage)
                .build();
    }

    private Long getEmpID(){
        Long empId = 1001L;
        Long maxId = employeeRepository.getMaxId();
        if(maxId > 0){
            empId = maxId + 1;
        }
        return empId;
    }

    public EmployeeListResponse getAll(int offset, int limit, String userId) throws ExceptionHandlerUtil {
        String userMessage = Messages.EMPLOYEE_DATA_NOT_FOUND;
        long itemCount = employeeRepository.count();
        List<EmployeeEntity> data = new ArrayList<EmployeeEntity>();

        if(itemCount > 0){
            Pageable pageable = PageRequest.of(offset, limit);
            Page<EmployeeEntity> page = employeeRepository.findAll(pageable);
            data = page.getContent();
            userMessage = Messages.SUCCESSFULLY_GET_EMPLOYEE_DATA;
        }

        return EmployeeListResponse.builder()
                .data(data)
                .count(itemCount)
                .userMessage(userMessage)
                .build();
    }
}
