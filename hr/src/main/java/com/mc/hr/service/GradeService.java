package com.mc.hr.service;

import com.mc.hr.entity.EmployeeEntity;
import com.mc.hr.entity.GradeEntity;
import com.mc.hr.repository.GradeRepository;
import com.mc.hr.request.LowestGradeRequest;
import com.mc.hr.response.CommonResponse;
import com.mc.hr.response.EmployeeListResponse;
import com.mc.hr.response.GradeListResponse;
import com.mc.hr.util.ExceptionHandlerUtil;
import com.mc.hr.util.IdGeneratorUtil;
import com.mc.hr.util.Messages;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mc.hr.util.Messages.DATA_NOT_FOUNT;

@Service
@Slf4j
public class GradeService {

    private static final Double basicSalaryIncrement = 5000.00;

    @Autowired
    GradeRepository gradeRepository;

    @Transactional
    public CommonResponse updateGradesBasic(LowestGradeRequest request, String userId) throws ExceptionHandlerUtil {
        String userMessage = Messages.SUCCESSFULLY_BASIC_SALARY_UPDATED;
        List<GradeEntity> gradeEntityList = gradeRepository.findAllByOrderByGradeNumberDesc();
        if(gradeEntityList == null || gradeEntityList.size() < 1){
            throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, DATA_NOT_FOUNT);
        }
        Double basicSalary = request.getBasicSalary();
        for(GradeEntity grade : gradeEntityList){
            grade.setBasic(basicSalary);
            grade.setUpdatedBy(userId);
            grade.setUpdatedOn(new Timestamp(new Date().getTime()));
            gradeRepository.save(grade);
            basicSalary += basicSalaryIncrement;
        }

        return CommonResponse.builder()
                .obj("success")
                .userMessage(userMessage)
                .build();
    }

    public GradeListResponse getAll(int offset, int limit, String userId) throws ExceptionHandlerUtil {
        String userMessage = Messages.GRADE_DATA_NOT_FOUND;
        long itemCount = gradeRepository.count();
        List<GradeEntity> data = new ArrayList<GradeEntity>();

        if(itemCount > 0){
            Pageable pageable = PageRequest.of(offset, limit);
            Page<GradeEntity> page = gradeRepository.findAll(pageable);
            data = page.getContent();
            userMessage = Messages.SUCCESSFULLY_GET_GRADE_DATA;
        }

        return GradeListResponse.builder()
                .data(data)
                .count(itemCount)
                .userMessage(userMessage)
                .build();
    }


}
