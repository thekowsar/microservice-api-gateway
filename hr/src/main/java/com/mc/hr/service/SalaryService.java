package com.mc.hr.service;

import com.google.common.base.Strings;
import com.mc.hr.entity.EmployeeEntity;
import com.mc.hr.entity.GradeEntity;
import com.mc.hr.entity.SalarySheetEntity;
import com.mc.hr.repository.EmployeeRepository;
import com.mc.hr.repository.GradeRepository;
import com.mc.hr.repository.SalarySheetRepository;
import com.mc.hr.request.CalculateSalaryRequest;
import com.mc.hr.response.CommonResponse;
import com.mc.hr.response.SalarySheetListResponse;
import com.mc.hr.util.ExceptionHandlerUtil;
import com.mc.hr.util.Messages;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SalaryService {

    private static final Double basicSalaryIncrement = 5000.00;

    @Autowired
    SalarySheetRepository salarySheetRepository;

    @Autowired
    GradeRepository gradeRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    private ResourceLoader resourceLoader;

    private static final String PDF_REPORT_PATH = "pdf-templates/";

    @Transactional
    public CommonResponse calculateSalary(CalculateSalaryRequest request, String userId) throws ExceptionHandlerUtil {
        String userMessage = Messages.SUCCESSFULLY_CALCULATE_SALARY;

        List<SalarySheetEntity> salarySheet = salarySheetRepository.findByMonthAndYear(request.getMonth(), request.getYear());
        if(salarySheet != null && salarySheet.size() > 0){
            return CommonResponse.builder()
                    .obj("success")
                    .userMessage(Messages.SUCCESSFULLY_ALREADY_CALCULATE_SALARY)
                    .build();
        }

        List<GradeEntity> gradeEntityList = gradeRepository.findAllByOrderByGradeNumberDesc();
        List<EmployeeEntity> employeeEntityList = employeeRepository.findAll();
        if(employeeEntityList == null || employeeEntityList.size() < 1){
            throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, "Employee list not found");
        }
        Map<String, GradeEntity> gradeEntityMap = this.getGradeMap(gradeEntityList);
        for(EmployeeEntity employeeEntity : employeeEntityList){
            GradeEntity gradeEntity = gradeEntityMap.get(employeeEntity.getGrade().getGradeOid());
            Double basic = gradeEntity.getBasic();
            Double houseRent = (basic * gradeEntity.getHouseRentInPer())/100;
            Double medicalAllowance = (basic * gradeEntity.getMedicalAllowanceInPer())/100;
            SalarySheetEntity salarySheetEntity = SalarySheetEntity.builder()
                    .month(request.getMonth())
                    .year(request.getYear())
                    .gradeNumber(gradeEntity.getGradeNumber())
                    .gradeName(gradeEntity.getGradeName())
                    .basic(basic)
                    .houseRentInPer(gradeEntity.getHouseRentInPer())
                    .houseRent(houseRent)
                    .medicalAllowanceInPer(gradeEntity.getMedicalAllowanceInPer())
                    .medicalAllowance(medicalAllowance)
                    .employeeOid(employeeEntity.getEmployeeOid())
                    .name(employeeEntity.getName())
                    .createdBy(userId)
                    .createdOn(new Timestamp(new Date().getTime()))
                    .build();

            salarySheetRepository.save(salarySheetEntity);
        }

        return CommonResponse.builder()
                .obj("success")
                .userMessage(userMessage)
                .build();
    }

    private Map<String, GradeEntity> getGradeMap(List<GradeEntity> gradeEntityList) {
        Map<String, GradeEntity> gradeEntityMap = gradeEntityList.stream()
                .collect( Collectors.toMap(GradeEntity::getGradeOid,
                        Function.identity()) );
        return gradeEntityMap;
    }

    public SalarySheetListResponse getSalarySheet(Integer month, Integer year, int offset, int limit, String userId) throws ExceptionHandlerUtil {
        String userMessage = Messages.SALARY_SHEET_DATA_NOT_FOUND;
        long itemCount = salarySheetRepository.countByMonthAndYear(month, year);
        List<SalarySheetEntity> data = new ArrayList<SalarySheetEntity>();

        if(itemCount > 0){
            Pageable pageable = PageRequest.of(offset, limit);
            Page<SalarySheetEntity> page = salarySheetRepository.findByMonthAndYear(month, year, pageable);
            data = page.getContent();
            userMessage = Messages.SUCCESSFULLY_GET_SALARY_SHEET;
        }

        return SalarySheetListResponse.builder()
                .data(data)
                .count(itemCount)
                .userMessage(userMessage)
                .build();

    }

    public void generatePdfFile(Integer month, Integer year, OutputStream out, String userId) throws ExceptionHandlerUtil {
        List<SalarySheetEntity> salarySheetEntityList = salarySheetRepository.findByMonthAndYear(month, year);
        Map<String, Object> parameters = new HashMap<String, Object>();
        if(salarySheetEntityList == null || salarySheetEntityList.size() < 1){
            throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, "Salary data not found for given month");
        }
        setParameters(month, year, userId, parameters);

        try {
            Resource resource = resourceLoader.getResource("classpath:" + PDF_REPORT_PATH + "statement.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(resource.getInputStream());
            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JRBeanCollectionDataSource(salarySheetEntityList));
            JasperExportManager.exportReportToPdfStream(print, out);
        } catch (FileNotFoundException e) {
            throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        } catch (IOException | JRException e) {
            throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    private void setParameters(Integer month, Integer year, String userId, Map<String, Object> parameters) {
        parameters.put("month", month);
        parameters.put("year", year);
        parameters.put("userId", userId);
    }




}
