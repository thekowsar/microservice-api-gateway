package com.mc.hr.service;

import com.mc.hr.entity.CompanyBankAccountEntity;
import com.mc.hr.entity.GradeEntity;
import com.mc.hr.repository.CompanyBankAccountRepository;
import com.mc.hr.repository.GradeRepository;
import com.mc.hr.request.CompanyAccountDepositRequest;
import com.mc.hr.request.LowestGradeRequest;
import com.mc.hr.response.CommonResponse;
import com.mc.hr.response.CompanyAccountResponse;
import com.mc.hr.util.ExceptionHandlerUtil;
import com.mc.hr.util.Messages;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static com.mc.hr.util.Messages.DATA_NOT_FOUNT;

@Service
@Slf4j
public class CompanyBankAccountService {

    private static final Double basicSalaryIncrement = 5000.00;

    @Autowired
    CompanyBankAccountRepository companyBankAccountRepository;

    public CommonResponse depositBalance(CompanyAccountDepositRequest request, String userId) throws ExceptionHandlerUtil {
        String userMessage = Messages.SUCCESSFULLY_COMPANY_BALANCE_DEPOSITED;
        CompanyBankAccountEntity companyAccount = companyBankAccountRepository.findByBankAccountNo(request.getBankAccountNo());
        if(companyAccount == null){
            throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, DATA_NOT_FOUNT);
        }
        companyAccount.setCurrentBalance(companyAccount.getCurrentBalance() + request.getAmount());
        companyAccount.setUpdatedBy(userId);
        companyAccount.setUpdatedOn(new Timestamp(new Date().getTime()));
        companyBankAccountRepository.save(companyAccount);

        return CommonResponse.builder()
                .obj("success")
                .userMessage(userMessage)
                .build();
    }



    public CompanyAccountResponse getCompanyAccount(String userId) throws ExceptionHandlerUtil {
        String userMessage = Messages.SUCCESSFULLY_GET_COMPANY_BANK_DATA;
        List<CompanyBankAccountEntity> companyBankAccountEntity = companyBankAccountRepository.findAll();
        if(companyBankAccountEntity == null || companyBankAccountEntity.size() < 1){
            throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, DATA_NOT_FOUNT);
        }

        return CompanyAccountResponse.builder()
                .companyBankAccountEntity(companyBankAccountEntity.get(0))
                .userMessage(userMessage)
                .build();
    }


}
