package com.mc.hr.repository;

import com.mc.hr.entity.EmployeeBankAccountEntity;
import com.mc.hr.entity.SalarySheetEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SalarySheetRepository extends JpaRepository<SalarySheetEntity, String> {

    List<SalarySheetEntity> findByMonthAndYear(Integer month, Integer year);

    Page<SalarySheetEntity> findByMonthAndYear(Integer month, Integer year, Pageable pageable);

    Long countByMonthAndYear(Integer month, Integer year);

}
