package com.mc.hr.repository;

import com.mc.hr.entity.GradeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GradeRepository extends JpaRepository<GradeEntity, String> {

    List<GradeEntity> findAllByOrderByGradeNumberDesc();

    GradeEntity findByGradeOid(String gradeOid);

}
