package com.mc.hr.repository;

import com.mc.hr.entity.EmployeeEntity;
import com.mc.hr.entity.GradeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeEntity, String> {

    @Query("SELECT coalesce(max(e.employeeId), 0) as maxId FROM EmployeeEntity e")
    Long getMaxId();

}
