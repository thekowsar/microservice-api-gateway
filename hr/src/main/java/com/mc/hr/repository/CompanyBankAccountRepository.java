package com.mc.hr.repository;

import com.mc.hr.entity.CompanyBankAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyBankAccountRepository extends JpaRepository<CompanyBankAccountEntity, String> {

    CompanyBankAccountEntity findByBankAccountNo(String bankAccountNo);

}
