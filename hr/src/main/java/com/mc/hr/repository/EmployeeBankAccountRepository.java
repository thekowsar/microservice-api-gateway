package com.mc.hr.repository;

import com.mc.hr.entity.EmployeeBankAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeBankAccountRepository extends JpaRepository<EmployeeBankAccountEntity, String> {

    EmployeeBankAccountEntity findByBankAccountNo(String bankAccountNo);

}
