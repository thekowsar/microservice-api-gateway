package com.mc.hr.util;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.UUID;

@Slf4j
@Component
public class IdGeneratorUtil {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    public Long generateUniqueId()
    {
        long val = -1;
        do
        {
            final UUID uid = UUID.randomUUID();
            final ByteBuffer buffer = ByteBuffer.wrap(new byte[16]);
            buffer.putLong(uid.getLeastSignificantBits());
            buffer.putLong(uid.getMostSignificantBits());
            final BigInteger bi = new BigInteger(buffer.array());
            val = bi.longValue();
        }
        // We also make sure that the ID is in positive space, if its not we simply repeat the process
        while (val < 0);
        return val;
    }

    public String getUserName(String bearerToken) {
        log.info("Token is : {}", bearerToken);
        String username = "";
        if(!Strings.isNullOrEmpty(bearerToken) && bearerToken.contains("Bearer")){
            bearerToken = bearerToken.substring(7);
        }
        username = jwtTokenUtil.getUsernameFromToken(bearerToken);
        log.info("Username is : {}", username);
        return username;
    }
}
