package com.mc.hr.util;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ExceptionHandlerUtil extends Exception{
    HttpStatus code;
    String message;
}
