package com.mc.hr.util;

public class Messages {

    public static final String DATA_NOT_FOUNT = "Data not found";
    public static final String SUCCESSFULLY_BASIC_SALARY_UPDATED = "Successfully basic salary updated";
    public static final String SUCCESSFULLY_COMPANY_BALANCE_DEPOSITED = "Successfully balance deposited";
    public static final String SUCCESSFULLY_EMPLOYEE_INFO_SAVED = "Successfully employee info saved";
    public static final String SUCCESSFULLY_CALCULATE_SALARY= "Successfully calculate salary";
    public static final String SUCCESSFULLY_ALREADY_CALCULATE_SALARY= "Already salary calculate";
    public static final String SUCCESSFULLY_GET_SALARY_SHEET = "Successfully get salary sheet";
    public static final String SALARY_SHEET_DATA_NOT_FOUND = "Salary sheet data not found";
    public static final String EMPLOYEE_DATA_NOT_FOUND = "Employee data not found";
    public static final String SUCCESSFULLY_GET_EMPLOYEE_DATA = "Successfully get employee data";
    public static final String GRADE_DATA_NOT_FOUND = "Grade data not found";
    public static final String SUCCESSFULLY_GET_GRADE_DATA = "Successfully get grade data";
    public static final String SUCCESSFULLY_GET_COMPANY_BANK_DATA = "Successfully get grade data";

}
