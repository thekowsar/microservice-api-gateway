package com.mc.hr.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString
@Table(name="Grade")
public class GradeEntity {

    @Id
    @Column(name = "gradeoid", updatable = false, nullable = false)
    private String gradeOid;

    @Column(name="gradenumber", unique = true)
    private Integer gradeNumber;

    @Column(name="gradename")
    private String gradeName;

    @Column(name="basic")
    private Double basic;

    @Column(name="houserentinper")
    private Double houseRentInPer;

    @Column(name="medicalallowanceinper")
    private Double medicalAllowanceInPer;

    @Column(name="createdby")
    private String createdBy;

    @Column(name="createdon")
    private Timestamp createdOn;

    @Column(name="updatedby")
    private String updatedBy;

    @Column(name="updatedon")
    private Timestamp updatedOn;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY, mappedBy = "grade")
    private Set<EmployeeEntity> employees;

}
