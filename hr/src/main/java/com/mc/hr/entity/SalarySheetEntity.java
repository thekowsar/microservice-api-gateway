package com.mc.hr.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString
@Table(name="Salarysheet")
public class SalarySheetEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "salarysheetoid", updatable = false, nullable = false)
    private String salarySheetOid;

    @Column(name="month")
    private Integer month;

    @Column(name="year")
    private Integer year;

    @Column(name="gradenumber")
    private Integer gradeNumber;

    @Column(name="gradename")
    private String gradeName;

    @Column(name="basic")
    private Double basic;

    @Column(name="houserentinper")
    private Double houseRentInPer;

    @Column(name="houserent")
    private Double houseRent;

    @Column(name="medicalallowanceinper")
    private Double medicalAllowanceInPer;

    @Column(name="medicalallowance")
    private Double medicalAllowance;

    @Column(name="employeeoid")
    private String employeeOid;

    @Column(name="name")
    private String name;

    @Column(name="createdby")
    private String createdBy;

    @Column(name="createdon")
    private Timestamp createdOn;

    @Column(name="updatedby")
    private String updatedBy;

    @Column(name="updatedon")
    private Timestamp updatedOn;

}
