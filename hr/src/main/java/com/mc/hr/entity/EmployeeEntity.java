package com.mc.hr.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name="Employee")
public class EmployeeEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "employeeoid", updatable = false, nullable = false)
    private String employeeOid;

    @Column(name="employeeid", unique = true)
    private Long employeeId;

    @Column(name="name")
    private String name;

    @Column(name="address")
    private String address;

    @Column(name="mobileno")
    private String mobileNo;

    @Column(name="createdby")
    private String createdBy;

    @Column(name="createdon")
    private Timestamp createdOn;

    @Column(name="updatedby")
    private String updatedBy;

    @Column(name="updatedon")
    private Timestamp updatedOn;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY, mappedBy = "employee")
    private Set<EmployeeBankAccountEntity> bankAccounts;

    @JsonBackReference
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "gradeoid", referencedColumnName="gradeoid")
    private GradeEntity grade;

    @Override
    public String toString() {
        return "Employee{" +
                "employeeOid='" + employeeOid + '\'' +
                ", employeeId='" + employeeId + '\'' +
                ", name=" + name +
                ", address=" + address +
                ", mobileNo=" + mobileNo +
                ", createdBy=" + createdBy +
                ", gradeOid=" + grade.getGradeOid() +
                '}';
    }

}
