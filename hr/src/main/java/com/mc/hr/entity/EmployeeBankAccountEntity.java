package com.mc.hr.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name="Employeebankaccount")
public class EmployeeBankAccountEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "bankaccountoid", updatable = false, nullable = false)
    private String bankAccountOid;

    @Column(name="bankaccountno", unique = true)
    private String bankAccountNo;

    @Column(name="accountname")
    private String accountName;

    @Column(name="accounttype")
    private String accountType;

    @Column(name="bankname")
    private String bankName;

    @Column(name="branchname")
    private String branchName;

    @Column(name="currentbalance")
    private Double currentBalance;

    @Column(name="createdby")
    private String createdBy;

    @Column(name="createdon")
    private Timestamp createdOn;

    @Column(name="updatedby")
    private String updatedBy;

    @Column(name="updatedon")
    private Timestamp updatedOn;

    @JsonBackReference
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "employeeoid", referencedColumnName="employeeoid")
    private EmployeeEntity employee;

    @Override
    public String toString() {
        return "Employee{" +
                "bankAccountOid='" + bankAccountOid + '\'' +
                ", bankAccountNo='" + bankAccountNo + '\'' +
                ", accountName=" + accountName +
                ", accountType=" + accountType +
                ", bankName=" + bankName +
                ", branchName=" + branchName +
                ", currentBalance=" + currentBalance +
                ", createdBy=" + createdBy +
                ", employeeoid=" + employee.getEmployeeOid() +
                '}';
    }

}
