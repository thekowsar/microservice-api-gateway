package com.mc.hr.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString
@Table(name="Companybankaccount")
public class CompanyBankAccountEntity {

    @Id
    @Column(name = "bankaccountoid", updatable = false, nullable = false)
    private String bankAccountOid;

    @Column(name="bankaccountno", unique = true)
    private String bankAccountNo;

    @Column(name="accountname")
    private String accountName;

    @Column(name="accounttype")
    private String accountType;

    @Column(name="bankname")
    private String bankName;

    @Column(name="branchname")
    private String branchName;

    @Column(name="currentbalance")
    private Double currentBalance;

    @Column(name="createdby")
    private String createdBy;

    @Column(name="createdon")
    private Timestamp createdOn;

    @Column(name="updatedby")
    private String updatedBy;

    @Column(name="updatedon")
    private Timestamp updatedOn;
}
