package com.mc.hr.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CommonResponse {

    private Object obj;

    private String userMessage;

}
