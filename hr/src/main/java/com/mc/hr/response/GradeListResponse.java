package com.mc.hr.response;

import com.mc.hr.entity.EmployeeEntity;
import com.mc.hr.entity.GradeEntity;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class GradeListResponse {

    private List<GradeEntity> data;
    private Long count;
    private String userMessage;

}
