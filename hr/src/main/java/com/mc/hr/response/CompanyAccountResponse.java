package com.mc.hr.response;

import com.mc.hr.entity.CompanyBankAccountEntity;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CompanyAccountResponse {

    private CompanyBankAccountEntity companyBankAccountEntity;
    private String userMessage;

}
