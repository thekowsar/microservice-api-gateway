package com.mc.hr.response;

import com.mc.hr.entity.SalarySheetEntity;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class SalarySheetListResponse {

    private List<SalarySheetEntity> data;
    private Long count;
    private String userMessage;

}
