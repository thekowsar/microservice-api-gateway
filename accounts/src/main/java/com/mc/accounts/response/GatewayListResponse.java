package com.mc.accounts.response;

import com.mc.accounts.entity.GatewayEntity;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class GatewayListResponse {

    private List<GatewayEntity> data;
    private Long count;
    private String userMessage;

}
