package com.mc.accounts.response;

import com.mc.accounts.entity.PeripheralDeviceEntity;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class GatewayResponse {

    private Object obj;

    private String userMessage;

    private Set<PeripheralDeviceEntity> peripheralDevices;

}
