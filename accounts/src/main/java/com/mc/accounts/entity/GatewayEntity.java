package com.mc.accounts.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString
@Table(name="Gateway")
public class GatewayEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "gatewayoid", updatable = false, nullable = false)
    private String gatewayOid;

    @Column(name="serialnumber", unique = true)
    private String serialNumber;

    @Column(name="gatewayname")
    private String gatewayName;

    @Column(name="ipv4address")
    private String ipv4Address;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY, mappedBy = "gateway")
    private Set<PeripheralDeviceEntity> peripheralDevices;

}
