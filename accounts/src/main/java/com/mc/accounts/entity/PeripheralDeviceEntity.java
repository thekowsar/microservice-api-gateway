package com.mc.accounts.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name="Peripheraldevice")
public class PeripheralDeviceEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "peripheraldeviceoid", updatable = false, nullable = false)
    private String peripheralDeviceOid;

    @Column(name="uid", unique = true)
    private Long uid;

    @Column(name="vendor")
    private String vendor;

    @Column(name="createdon")
    private Timestamp createdOn;

    @Column(name="status")
    private String status;

    @JsonBackReference
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "gatewayoid", referencedColumnName="gatewayoid")
    private GatewayEntity gateway;

    @Override
    public String toString() {
        return "Peripheraldevice{" +
                "peripheralDeviceOid='" + peripheralDeviceOid + '\'' +
                ", uid='" + uid + '\'' +
                ", vendor=" + vendor +
                ", createdOn=" + createdOn +
                ", status=" + status +
                ", gatewayOid=" + gateway.getGatewayOid() +
                '}';
    }

}
