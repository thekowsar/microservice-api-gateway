package com.mc.accounts.service;

import com.mc.accounts.request.GatewayRequest;
import com.mc.accounts.response.GatewayListResponse;
import com.mc.accounts.util.ExceptionHandlerUtil;
import com.mc.accounts.util.Messages;
import com.mc.accounts.entity.GatewayEntity;
import com.mc.accounts.repository.GatewayRepository;
import com.mc.accounts.response.GatewayResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class GatewayService {

    @Autowired
    GatewayRepository gatewayRepository;

    public GatewayResponse getByOid(String oid) throws ExceptionHandlerUtil {
        String userMessage = Messages.SUCCESSFULLY_GATEWAY_FOUND;
        GatewayEntity gatewayEntity = gatewayRepository.findByGatewayOid(oid);
        if(gatewayEntity == null){
            userMessage = String.format(Messages.GATEWAY_NOT_EXIST_BY_OID, oid);
        }

        return GatewayResponse.builder()
                .obj(gatewayEntity)
                .userMessage(userMessage)
                .build();
    }

    public GatewayEntity save(GatewayRequest request) throws ExceptionHandlerUtil{
        GatewayEntity gatewayEntity = new GatewayEntity();
        GatewayEntity isExist = gatewayRepository.findBySerialNumber(request.getSerialNumber());
        if(isExist != null){
            log.error("Gateway already exist with this serial number: {}", request.getSerialNumber());
            throw new ExceptionHandlerUtil(HttpStatus.FOUND, String.format(Messages.GATEWAY_EXIST_BY_SERIAL_NO, request.getSerialNumber()));
        }
        try{
            BeanUtils.copyProperties(request, gatewayEntity);
            gatewayEntity = gatewayRepository.save(gatewayEntity);
        } catch (Exception e){
            log.error("Failed to save gateway data {} for {}", request, e.getMessage());
            throw new ExceptionHandlerUtil(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return gatewayEntity;
    }

    public GatewayListResponse findAll(int offset, int limit) throws ExceptionHandlerUtil{
        long itemCount = gatewayRepository.count();
        String userMessage = Messages.DATA_NOT_FOUNT;
        List<GatewayEntity> data = new ArrayList<>();
        if(itemCount > 0){
            Pageable pageable = PageRequest.of(offset, limit);
            Page<GatewayEntity> page = gatewayRepository.findAll(pageable);
            data = page.getContent();
            userMessage = Messages.SUCCESSFULLY_GATEWAY_LIST_FOUND;
        }
        return GatewayListResponse.builder()
                .data(data)
                .count(itemCount)
                .userMessage(userMessage)
                .build();
    }

}
