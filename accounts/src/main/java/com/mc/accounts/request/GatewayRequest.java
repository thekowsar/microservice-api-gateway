package com.mc.accounts.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class GatewayRequest {

    @NotBlank(message = "SerialNumber can not be empty")
    private String serialNumber;

    @NotBlank(message = "GatewayName can not be empty")
    private String gatewayName;

    @NotBlank(message = "Ipv4Address can not be empty")
    @Pattern(regexp = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$", message = "Invalid IPv4 address")
    private String ipv4Address;

}
