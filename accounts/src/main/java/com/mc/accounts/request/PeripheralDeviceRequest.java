package com.mc.accounts.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class PeripheralDeviceRequest {

    @NotBlank(message = "Vendor can not be null or empty")
    private String vendor;

    @NotBlank(message = "GatewayOid can not be null or empty")
    private String gatewayOid;

    @NotBlank(message = "Status can not be null or empty")
    @Pattern(regexp = "(online)|(offline)", message = "Status must be in online or offline")
    private String status;

}
