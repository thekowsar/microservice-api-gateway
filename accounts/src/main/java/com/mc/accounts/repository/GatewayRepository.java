package com.mc.accounts.repository;

import com.mc.accounts.entity.GatewayEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GatewayRepository extends JpaRepository<GatewayEntity, String> {

    GatewayEntity findByGatewayOid(String gatewayOid);

    GatewayEntity findBySerialNumber(String serialNumber);

    Page<GatewayEntity> findAll(Pageable pageable);

}
