package com.mc.accounts.repository;

import com.mc.accounts.entity.PeripheralDeviceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PeripheralDeviceRepository extends JpaRepository<PeripheralDeviceEntity, String> {

    PeripheralDeviceEntity findByPeripheralDeviceOid(String peripheralDeviceOid);

}
