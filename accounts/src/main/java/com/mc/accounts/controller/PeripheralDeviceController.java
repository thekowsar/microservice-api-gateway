package com.mc.accounts.controller;

import com.mc.accounts.entity.GatewayEntity;
import com.mc.accounts.request.PeripheralDeviceRequest;
import com.mc.accounts.service.PeripheralDeviceService;
import com.mc.accounts.util.ExceptionHandlerUtil;
import com.mc.accounts.response.GatewayResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@Validated
@Slf4j
@RequestMapping("/v1/peripheral/device")
public class PeripheralDeviceController {

    @Autowired
    PeripheralDeviceService peripheralDeviceService;

    @PostMapping(value = "/save")
    public ResponseEntity<GatewayEntity> save(@Valid @RequestBody PeripheralDeviceRequest request) throws Exception {
        try{
            log.info("Request received to save peripheral device: {}", request);
            GatewayEntity response = peripheralDeviceService.add(request);
            log.info("Successfully peripheral device saved {}", response);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }

    @DeleteMapping(value = "/delete/{oid}")
    public ResponseEntity<GatewayResponse> delete(@PathVariable("oid") @NotBlank String oid) throws Exception {
        try{
            log.info("Request received to delete peripheral device by oid: {}", oid);
            GatewayResponse response = peripheralDeviceService.deleteByOid(oid);
            log.info("Successfully peripheral device deleted by oid {}", oid);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }
}
