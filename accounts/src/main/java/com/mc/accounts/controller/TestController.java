package com.mc.accounts.controller;


import com.mc.accounts.response.GatewayResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@Slf4j
@RequestMapping("/v1/test")
public class TestController {

    @GetMapping(value = "/res")
    public ResponseEntity<GatewayResponse> getByOid() throws Exception {
            log.info("Get gateway by oid : ");
        GatewayResponse res = GatewayResponse.builder()
                .userMessage("test Account User message")
                .build();
            return new ResponseEntity<GatewayResponse>(res, HttpStatus.OK);
    }
}
