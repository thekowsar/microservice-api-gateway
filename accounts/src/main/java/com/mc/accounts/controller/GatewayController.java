package com.mc.accounts.controller;

import com.mc.accounts.entity.GatewayEntity;
import com.mc.accounts.request.GatewayRequest;
import com.mc.accounts.response.GatewayListResponse;
import com.mc.accounts.response.GatewayResponse;
import com.mc.accounts.service.GatewayService;
import com.mc.accounts.util.ExceptionHandlerUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Data
@RestController
@Validated
@Slf4j
@RequestMapping("/v1/gateway")
public class GatewayController {

    @Autowired
    GatewayService gatewayService;

    @GetMapping(value = "/get-by-oid/{oid}")
    public ResponseEntity<GatewayResponse> getByOid(@PathVariable("oid") @NotBlank String oid) throws Exception {
        try{
            log.info("Get gateway by oid : {}", oid);
            GatewayResponse response = gatewayService.getByOid(oid);
            log.info("Successfully gateway information get : {}", response);
            return new ResponseEntity<GatewayResponse>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }

    @GetMapping(value = "/get-all")
    public ResponseEntity<GatewayListResponse> getAll(
            @RequestParam(name = "offset", required = false, defaultValue = "0") int offset,
            @RequestParam(name = "limit", required = false, defaultValue = "10") int limit) throws Exception {
        try{
            log.info("Get all gateway request");
            GatewayListResponse response = gatewayService.findAll(offset, limit);
            log.info("Successfully gateway information get : {}", response.getCount());
            return new ResponseEntity<GatewayListResponse>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex);
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }

    @PostMapping(value = "/save")
    public ResponseEntity<GatewayEntity> save(@Valid @RequestBody GatewayRequest request) throws Exception {
        try{
            log.info("Request received to create gateway: {}", request);
            GatewayEntity response = gatewayService.save(request);
            log.info("Gateway successfully saved {}", response);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (ExceptionHandlerUtil ex){
            log.error(ex.getMessage(), ex.getMessage());
            throw new ResponseStatusException(ex.getCode(), ex.getMessage(), ex);
        }
    }
}
