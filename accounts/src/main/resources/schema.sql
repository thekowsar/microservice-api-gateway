drop table if exists TransLog;

create table                   TransLog
(
transLogOid                    varchar(128)                                                 not null,
transType                      varchar(16)                                                  not null,
transAmount                    numeric(20, 6)                                               not null,
fromAccount                    varchar(16),
toAccount                      varchar(16)                                                  not null,
employeeId                     varchar(16),
transDate                      timestamp                                                    not null,
createdBy                      varchar(128)                                                 not null,
createdOn                      timestamp                                                    not null,
updatedBy                      varchar(128),
updatedOn                      timestamp,
constraint                     pk_trans_log                                                 primary key    (transLogOid)
);