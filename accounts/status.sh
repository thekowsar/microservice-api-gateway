#!/bin/bash

name="ACCOUNTS"
ps aux | grep accounts-0.0.1
id=`echo $(ps aux | grep accounts-0.0.1 | grep -v grep | awk '{print $2}')`
[[ -z "$id" ]] && echo $name "Application is not running" || echo $name "Application is running, PID is = " $id
