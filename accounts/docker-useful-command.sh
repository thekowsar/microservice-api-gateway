#!/usr/bin/env bash

NETWORK_ID=$(echo `docker network ls | grep oaas_net | awk '{print $1}'`)

if [ ! -z "${NETWORK_ID}" ]
then
    echo `docker network ls | grep oaas_net`
else
    docker network create --driver=bridge --subnet=162.17.1.0/24 --gateway=162.17.1.1 gateway_net
    echo "oaas_net network created"
    echo `docker network ls | grep oaas_net`
fi


# pip3 install -r scripts/requirements.txt --user

# docker stop $(docker ps -a | grep con_ump_bi | awk '{print $1}')
# docker rm $(docker ps -a | grep con_ump_bi | awk '{print $1}')
# docker rmi $(docker images | grep img_ump_bi | tr -s ' ' | cut -d ' ' -f 3) -f
# docker rmi $(docker images | grep none | tr -s ' ' | cut -d ' ' -f 3) -f
# docker build --tag=img_ump_bi .
# docker build --tag={image_name}:{tag_name} --build-arg CONTAINER_PORT={container_port} ."
# docker run --name=con_ump_bi -it -d -v $(pwd)/data:/app/data -p 3838:3838 img_ump_bi
# docker run --name={container_name} -it -d -v {container_log}:/app/log -v /opt/brownfield/data:/opt/brownfield/data --net {docker_network} --ip {container_host} {image_name}:{tag_name}
# docker exec -it con_ump_bi sh

# docker volume create portainer_data
# docker run --name=portainer -it -d -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data --net oaas_net --ip 162.17.1.20 portainer/portainer-ce
# docker restart <portainer_id>
# sudo systemctl restart docker
# sudo systemctl status docker
# sudo systemctl start docker
