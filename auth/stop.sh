#!/bin/bash

name="AUTH"
echo "Collect running pid of auth-0.0.1 application"
id=`echo $(ps aux | grep auth-0.0.1 | grep -v grep | awk '{print $2}')`

[[ -z "$id" ]] && echo $name "Application is not running" || echo $name "Application PID is = " $id
[[ -z "$id" ]] || echo $name "Application stopping ..."
[[ -z "$id" ]] || kill -9 $id
