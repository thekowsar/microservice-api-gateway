drop table if exists User;

create table                   User
(
id                             varchar(258)                                           not null,
username                       varchar(128)                                           not null,
password                       varchar(1024)                                           not null,
constraint                     pk_user                                                primary key    (id)
);

INSERT INTO User (id, username, password) VALUES ('100001', 'zara', '$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6');
