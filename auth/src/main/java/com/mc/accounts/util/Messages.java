package com.mc.accounts.util;

public class Messages {
    public static final String DATA_NOT_FOUNT = "Data not found";
    public static final String SUCCESSFULLY_GATEWAY_LIST_FOUND = "Successfully get gateway list";
    public static final String SUCCESSFULLY_GATEWAY_FOUND = "Successfully get gateway";
    public static final String GATEWAY_EXIST_BY_SERIAL_NO = "Gateway already exist with this serial number %s";
    public static final String DEVICE_NOT_EXIST_BY_OID = "Peripheral device not exist by oid: %s";
    public static final String DEVICE_DELETED_BY_OID = "Successfully peripheral device deleted by oid: %s";
    public static final String GATEWAY_NOT_EXIST_BY_OID = "Gateway dose not exist with oid: %s";
    public static final String GATEWAY_ALREADY_HAVE_10_DEVICE = "Already 10 peripheral device added into this gateway: %s";
}
