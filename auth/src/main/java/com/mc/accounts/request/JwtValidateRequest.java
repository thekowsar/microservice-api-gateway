package com.mc.accounts.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class JwtValidateRequest {

    private String token;

}
