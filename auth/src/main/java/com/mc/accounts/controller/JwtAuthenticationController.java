package com.mc.accounts.controller;

import com.mc.accounts.config.JwtTokenUtil;
import com.mc.accounts.request.JwtRequest;
import com.mc.accounts.request.JwtValidateRequest;
import com.mc.accounts.response.JwtResponse;
import com.mc.accounts.response.JwtValidateResponse;
import com.mc.accounts.service.JwtUserDetailsService;
import com.netflix.servo.util.Strings;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @RequestMapping(value = "/validate", method = RequestMethod.POST)
    public ResponseEntity<?> validateAuthenticationToken(@RequestBody JwtValidateRequest validateRequest) throws Exception {
        log.info("Token validation request: {}", validateRequest);
        String username, jwtToken;
        JwtValidateResponse response = JwtValidateResponse.builder()
                .jwtToken(validateRequest.getToken())
                .doseValid(true)
                .message("Valid token")
                .build();

        if (validateRequest == null && !Strings.isNullOrEmpty(validateRequest.getToken())) {
            log.error("JWT Token does not exist in request");
            return ResponseEntity.ok(getValidateFailResponse("JWT Token does not exist in request"));
        }

        jwtToken = validateRequest.getToken();

        try {
            username = jwtTokenUtil.getUsernameFromToken(jwtToken);
        } catch (IllegalArgumentException e) {
            log.error("Unable to get JWT Token", e);
            return ResponseEntity.ok(getValidateFailResponse("Unable to get JWT Token"));
        } catch (ExpiredJwtException e) {
            log.error("JWT Token has expired", e);
            return ResponseEntity.ok(getValidateFailResponse("JWT Token has expired"));
        } catch (MalformedJwtException e) {
            log.error("Malformed Jwt token", e);
            return ResponseEntity.ok(getValidateFailResponse("Malformed Jwt token"));
        } catch (Exception e) {
            log.error("Failed to parse Jwt token", e);
            return ResponseEntity.ok(getValidateFailResponse(e.getMessage()));
        }

        if (Strings.isNullOrEmpty(username)) {
            log.error("Username not found for this token");
            return ResponseEntity.ok(getValidateFailResponse("Username not found for this token"));
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        if (!jwtTokenUtil.validateToken(jwtToken, userDetails)) {
            log.error("JWT Token does not exist in request");
            return ResponseEntity.ok(getValidateFailResponse("Invalid JWT Token"));
        }
        log.info("Auth Service response: {}", response);
        return ResponseEntity.ok(response);
    }

    private JwtValidateResponse getValidateFailResponse(String message) {
        return JwtValidateResponse.builder()
                .jwtToken("")
                .doseValid(false)
                .message(message)
                .build();
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}
