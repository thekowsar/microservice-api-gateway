package com.mc.accounts.repository;

import com.mc.accounts.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer   > {

    UserEntity findByUsername(String username);

}
