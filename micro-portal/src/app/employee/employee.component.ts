import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { EmployeeInfo, EmployeeListResponse, CommonResponse, SaveEmployee, HttpclientService } from '../service/httpclient.service';
import { NotificationService } from '../service/notification.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  employeeForm: NgForm;
  saveEmployee: SaveEmployee = new SaveEmployee("", "", "", "", "", "", "", "", "");
  totalCount: number; serial: number;
  employeeResponse:EmployeeListResponse;
  employees:EmployeeInfo[] = [];
  loading: boolean = true;
  displayedColumns: string[] = ['employeeId', 'name', 'mobileNo', 'address', 'bankAccountNo'];
  dataSource = new MatTableDataSource<any>();


  grades: any = [
    {'key': '1', 'value': 'Grade One'},
    {'key': '2', 'value': 'Grade Two'},
    {'key': '3', 'value': 'Grade Three'},
    {'key': '4', 'value': 'Grade Four'},
    {'key': '5', 'value': 'Grade Five'},
    {'key': '6', 'value': 'Grade Six'}
  ]
  accountTypes: any = [
    {'key': 'Current', 'value': 'Current'},
    {'key': 'Savings', 'value': 'Savings'}
  ]

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private httpClient: HttpclientService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.httpClient.getEmployeeInfo("0", "10").subscribe(
      response => this.handleResponseGetEmp(response)
    );
  }
  handleErrorGetEmp(errorMessage){
    //this.notificationService.showError(errorMessage);
    //this.deviceCreate = new DeviceCreate("", "online", "");
    console.log("Grades Error Response :");
    console.log(errorMessage);
  }

  handleResponseGetEmp(response:EmployeeListResponse){
    this.loading = false;
    this.employees = response.data;
    this.employees.length = response.count;
    this.dataSource = new MatTableDataSource<any>(this.employees);
    this.dataSource.paginator = this.paginator;
  }


  pageChanged(event){
   // this.loading = true;

    let pageIndex = event.pageIndex;
    let pageSize = event.pageSize;

    let previousIndex = event.previousPageIndex;

    let previousSize = pageSize * pageIndex;

    this.getNextData(previousSize, (pageIndex).toString(), pageSize.toString());
  }

  getNextData(currentSize, offset, limit){
    this.httpClient.getEmployeeInfo(offset, limit).subscribe(
      response => {
        this.loading = false;
        this.employees.length = currentSize;
        this.employees.push(...response.data);
        this.employees.length = response.count;
        this.dataSource = new MatTableDataSource<any>(this.employees);
        this.dataSource._updateChangeSubscription();
        this.dataSource.paginator = this.paginator;
      }
    );
  }



  createEmployee(){
    this.httpClient.saveEmployeeInfo(this.saveEmployee).subscribe(response =>{
      this.handleResponse(response);
    },
    error => this.handleError(error.error.message)
    );
  }

  handleError(errorMessage:String){
    this.notificationService.showError(errorMessage);
    this.saveEmployee = new SaveEmployee("", "", "", "", "", "", "", "", "");
  }

  handleResponse(response:CommonResponse){
    this.notificationService.showSuccess(response.userMessage);
    this.saveEmployee = new SaveEmployee("", "", "", "", "", "", "", "", "");

    this.httpClient.getEmployeeInfo("0", "10").subscribe(
      response => this.handleResponseGetEmp(response)
    );
    
  }

}
