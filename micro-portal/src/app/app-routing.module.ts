import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GradeComponent } from './grade/grade.component';
import { CompanyComponent } from './company/company.component';
import { EmployeeComponent } from './employee/employee.component';
import { SalaryComponent } from './salary/salary.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthGaurdService } from './service/auth-gaurd.service';

const routes: Routes = [
  {path:'', redirectTo: "/grade", pathMatch: 'full'},
  {path: "login", component: LoginComponent},
  {path: "logout", component: LogoutComponent, canActivate:[AuthGaurdService]},
  {path: "grade", component: GradeComponent ,canActivate:[AuthGaurdService]},
  {path: "company", component: CompanyComponent ,canActivate:[AuthGaurdService]},
  {path: "employee", component: EmployeeComponent ,canActivate:[AuthGaurdService]},
  {path: "salary", component: SalaryComponent ,canActivate:[AuthGaurdService]},
  {path: "**", component: PageNotFoundComponent ,canActivate:[AuthGaurdService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
