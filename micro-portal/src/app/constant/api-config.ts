import { environment } from 'src/environments/environment';

export class APIConfig {
    static BASE_API_URL = environment.baseUrl + environment.port;
}

export class GatewayApi {
    static GATEWAY_GET_ALL_URL = APIConfig.BASE_API_URL + '/v1/gateway/get-all';
    static GATEWAY_SAVE_URL = APIConfig.BASE_API_URL + '/v1/gateway/save';
    static GATEWAY_GET_BY_OID_URL = APIConfig.BASE_API_URL + '/v1/gateway/get-by-oid/';
    static DEVICE_SAVE_URL = APIConfig.BASE_API_URL + '/v1/peripheral/device/save';
    static DEVICE_DELETE_URL = APIConfig.BASE_API_URL + '/v1/peripheral/device/delete/';
    static USER_LOGIN_URL = APIConfig.BASE_API_URL + '/api/auth/authenticate';
    static GAT_ACCOUNT_INFO = APIConfig.BASE_API_URL + '/api/accounts/v1/test/res';

    static GRADE_GET_ALL_URL = APIConfig.BASE_API_URL + '/api/hr/v1/grade/info/get-all';
    static GRADE_UPDATE_BASIC_URL = APIConfig.BASE_API_URL + '/api/hr/v1/grade/basic/lowest-grade';
    static COMPAN_BANK_ACCOUNT_GET_URL = APIConfig.BASE_API_URL + '/api/hr/v1/company/account/get';
    static COMPAN_BANK_ACCOUNT_DEPOSIT_PUT_URL = APIConfig.BASE_API_URL + '/api/hr/v1/company/account/deposit';
    static EMPLOYEE_INFO_SAVE_URL = APIConfig.BASE_API_URL + '/api/hr/v1/employee/info/save';
    static EMPLOYEE_INFO_GET_URL = APIConfig.BASE_API_URL + '/api/hr/v1/employee/info/get-all';
    static CALCULATE_SALARY_URL = APIConfig.BASE_API_URL + '/api/hr/v1/salary/sheet/calculate';
    static SALARY_SHEET_GET_URL = APIConfig.BASE_API_URL + '/api/hr/v1/salary/sheet/get';

}

