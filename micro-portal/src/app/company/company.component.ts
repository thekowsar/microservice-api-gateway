import { Component, OnInit } from '@angular/core';
import { CommonResponse, CompanyAccountInfo, CompanyBankAccountEntity, Deposit, HttpclientService } from '../service/httpclient.service';
import { NotificationService } from '../service/notification.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

  loading: boolean = true;
  companyAccountInfo: CompanyBankAccountEntity = new CompanyBankAccountEntity ("", "", "", "", "", "", "", "", "", "", "");
  depositAmount:string;

  constructor(
    private httpClientService:HttpclientService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.httpClientService.getCompanyBankAccountInfo().subscribe(response =>{
      this.handleResponse(response);
    },
    error => this.handleError(error.error.message)
    );
  }

  handleError(errorMessage){
    //this.notificationService.showError(errorMessage);
    //this.deviceCreate = new DeviceCreate("", "online", "");
    console.log("Company Accoount Error Response :");
    console.log(errorMessage);
  }

  handleResponse(response:CompanyAccountInfo){
    this.loading = false;
    this.companyAccountInfo = response.companyBankAccountEntity;
  }

  deposit(){
    if(this.depositAmount == undefined || this.depositAmount == null || this.depositAmount == ""){
      this.notificationService.showError("Deposit Amount can not be empty");
      return;
    }
    this.httpClientService.depositToCompanyAccount(new Deposit(this.depositAmount, this.companyAccountInfo.bankAccountNo)).subscribe(response =>{
      this.handleResponseDeposit(response);
    },
    error => this.handleErrorDeposit(error.error.message)
    );
  }

  handleErrorDeposit(errorMessage){
    debugger;
    this.loading = false;
    this.notificationService.showError(errorMessage);
    this.depositAmount = "";
  }

  handleResponseDeposit(response:CommonResponse){
    debugger
    this.notificationService.showSuccess(response.userMessage);
    console.log(response)
    this.loading = false;
    this.depositAmount = "";

    this.httpClientService.getCompanyBankAccountInfo().subscribe(response =>{
      this.handleResponse(response);
    },
    error => this.handleError(error.error.message)
    );
  }

}
