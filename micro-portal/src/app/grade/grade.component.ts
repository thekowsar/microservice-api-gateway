import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { BasicSalary, CommonResponse, Gateway, GatewayResponse, Grade, GradeResponse, HttpclientService } from '../service/httpclient.service';
import { NotificationService } from '../service/notification.service';

@Component({
  selector: 'app-grade',
  templateUrl: './grade.component.html',
  styleUrls: ['./grade.component.css']
})
export class GradeComponent implements OnInit {
  lowestBasic: string;
  totalCount: number; serial: number;
  gatewayResponse:GatewayResponse;
  grades:Grade[] = [];
  loading: boolean = true;

  displayedColumns: string[] = ['gradeNumber', 'gradeName', 'basic', 'houseRentInPer', 'medicalAllowanceInPer'];

  //dataSource = this.gateways;
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private httpClientService:HttpclientService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.httpClientService.getGrades("0", "10").subscribe(
      response => this.handleResponse(response)
    );
  }



  handleError(errorMessage){
    //this.notificationService.showError(errorMessage);
    //this.deviceCreate = new DeviceCreate("", "online", "");
    console.log("Grades Error Response :");
    console.log(errorMessage);
  }

  handleResponse(response:GradeResponse){
    this.loading = false;
    this.grades = response.data;
    this.grades.length = response.count;
    this.dataSource = new MatTableDataSource<any>(this.grades);
    this.dataSource.paginator = this.paginator;
  }


  pageChanged(event){
   // this.loading = true;

    let pageIndex = event.pageIndex;
    let pageSize = event.pageSize;

    let previousIndex = event.previousPageIndex;

    let previousSize = pageSize * pageIndex;

    this.getNextData(previousSize, (pageIndex).toString(), pageSize.toString());
  }

  getNextData(currentSize, offset, limit){
    this.httpClientService.getGrades(offset, limit).subscribe(
      response => {
        this.loading = false;
        this.grades.length = currentSize;
        this.grades.push(...response.data);
        this.grades.length = response.count;
        this.dataSource = new MatTableDataSource<any>(this.grades);
        this.dataSource._updateChangeSubscription();
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  updateBasic(){
    if(this.lowestBasic == undefined || this.lowestBasic == null || this.lowestBasic == ""){
      this.notificationService.showSuccess("Basic salary can not be empty");
      return;
    }
    this.httpClientService.updateBasicSalary(new BasicSalary(this.lowestBasic)).subscribe(response =>{
      this.handleResponseBasicSalaryUpdate(response);
    },
    error => this.handleErrorBasicSalaryUpdate(error.error.message)
    );
  }

  handleErrorBasicSalaryUpdate(errorMessage:String){
    this.notificationService.showError(errorMessage);
    this.lowestBasic = "";
  }

  handleResponseBasicSalaryUpdate(response:CommonResponse){
    this.notificationService.showSuccess("Basic salary successfully");
    this.lowestBasic = "";
    this.httpClientService.getGrades("0", "10").subscribe(
      response => this.handleResponse(response)
    );
  }

}
