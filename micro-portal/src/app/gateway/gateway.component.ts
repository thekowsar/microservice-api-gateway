import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Gateway, GatewayResponse, GatewayByOidResponse, HttpclientService } from '../service/httpclient.service';
import { Router } from '@angular/router';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import {FormControl} from '@angular/forms';
import { DialogDeviceListComponent } from '../dialog-device-list/dialog-device-list.component';

@Component({
  selector: 'app-gateway',
  templateUrl: './gateway.component.html',
  styleUrls: ['./gateway.component.css']
})
export class GatewayComponent implements OnInit {
  totalCount: number; serial: number;
  gatewayResponse:GatewayResponse;
  gateways:Gateway[] = [];
  //loading: boolean = true;
  loading: boolean = false;

  displayedColumns: string[] = ['serialNumber', 'gatewayName', 'ipv4Address', 'actions', 'devicelist'];
  //dataSource = this.gateways;
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private httpClientService:HttpclientService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    /*
    this.httpClientService.getGateways("0", "5").subscribe(
      response => this.handleResponse(response)
    );
    */
    this.httpClientService.getAccountsRes().subscribe(response => {
      this.handleAccountResponse(response)
    },
    error => this.handleError(error)
    );
  }

  handleAccountResponse(response: GatewayByOidResponse){
    console.log("Accounts Response :");
    console.log(response);
  }

  handleError(errorMessage){
    //this.notificationService.showError(errorMessage);
    //this.deviceCreate = new DeviceCreate("", "online", "");
    console.log("Accounts Error Response :");
    console.log(errorMessage);
  }

  handleResponse(response:GatewayResponse){
    this.loading = false;
    this.gateways = response.data;
    this.gateways.length = response.count;
    this.dataSource = new MatTableDataSource<any>(this.gateways);
    this.dataSource.paginator = this.paginator;
  }

  getNextData(currentSize, offset, limit){

    this.httpClientService.getGateways(offset, limit).subscribe(
      response => {
        this.loading = false;
        this.gateways.length = currentSize;
      this.gateways.push(...response.data);
        //this.gateways = response.data;
        this.gateways.length = response.count;
        this.dataSource = new MatTableDataSource<any>(this.gateways);
        this.dataSource._updateChangeSubscription();
        this.dataSource.paginator = this.paginator;
      }
    );
  }


  pageChanged(event){
   // this.loading = true;

    let pageIndex = event.pageIndex;
    let pageSize = event.pageSize;

    let previousIndex = event.previousPageIndex;

    let previousSize = pageSize * pageIndex;

    this.getNextData(previousSize, (pageIndex).toString(), pageSize.toString());
  }
  

  onSelect(gateway:Gateway){
    this.router.navigate(["/gateways", gateway.gatewayOid]);
    // this.router.navigate([gateway.gatewayOid], {relativeTo: this.router})
  }

  animal: string;
  name: string;

  openDialog(gateway:Gateway): void {
    const dialogRef = this.dialog.open(DialogDeviceListComponent, {
      width: '1000px',
      data: gateway
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

}
