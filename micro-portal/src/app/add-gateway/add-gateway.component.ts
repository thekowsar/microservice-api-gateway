import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Gateway, GatewayCreate, HttpclientService } from '../service/httpclient.service';
import { NotificationService } from '../service/notification.service';

@Component({
  selector: 'app-add-gateway',
  templateUrl: './add-gateway.component.html',
  styleUrls: ['./add-gateway.component.css']
})
export class AddGatewayComponent implements OnInit {

  gatewayForm: NgForm;

  gatewayCreate: GatewayCreate = new GatewayCreate("", "", "");

  constructor(
    private httpClient: HttpclientService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
  }

  createGateway(){
    this.httpClient.createGateway(this.gatewayCreate).subscribe(response =>{
      this.handleResponse(response);
    },
    error => this.handleError(error.error.message)
    );
  }

  handleError(errorMessage:String){
    this.notificationService.showError(errorMessage);
    this.gatewayCreate = new GatewayCreate("", "", "");
  }

  handleResponse(response:Gateway){
    this.notificationService.showSuccess("Gateway created successfully");
    this.gatewayCreate = new GatewayCreate("", "", "");
    
  }

}
