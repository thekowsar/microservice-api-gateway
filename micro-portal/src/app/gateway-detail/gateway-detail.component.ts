import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Gateway, GatewayByOidResponse, HttpclientService, PeripheralDevice, DeviceCreate, DeviceDeletedResponse } from '../service/httpclient.service';
import { NotificationService } from '../service/notification.service';

@Component({
  selector: 'app-gateway-detail',
  templateUrl: './gateway-detail.component.html',
  styleUrls: ['./gateway-detail.component.css']
})
export class GatewayDetailComponent implements OnInit {

  deviceCreate: DeviceCreate = new DeviceCreate("", "online", "");

  public gatewayOid;
  public gateway: Gateway = new Gateway("", "", "", "", []);
  public peripheralDevices: PeripheralDevice[];

  statusList: any = ['online', 'offline']


  constructor(
    private route: ActivatedRoute,
    private httpClient: HttpclientService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.gatewayOid = this.route.snapshot.paramMap.get("id");
    this.httpClient.getGatewayByOid(this.gatewayOid).subscribe(response => {
        this.handleResponse(response)
    });
  }

  handleResponse(response:GatewayByOidResponse){
    this.gateway = response.obj;
    this.peripheralDevices = this.gateway.peripheralDevices;
  }

  createDevice(){
    this.deviceCreate.gatewayOid = this.gatewayOid;
    this.httpClient.createDevice(this.deviceCreate).subscribe(response =>{
      this.handleDeviceCreateResponse(response);
    },
    error => this.handleError(error.error.message)
    );
  }

  handleError(errorMessage:String){
    this.notificationService.showError(errorMessage);
    this.deviceCreate = new DeviceCreate("", "online", "");
  }

  handleDeviceCreateResponse(response:Gateway){
    this.notificationService.showSuccess("Device created successfully");
    this.gateway = response;
    this.peripheralDevices = this.gateway.peripheralDevices;
    this.deviceCreate = new DeviceCreate("", "online", "");
  }

  deleteDebice(device:PeripheralDevice){
    this.httpClient.deleteDevice(device.peripheralDeviceOid).subscribe(response => {
      this.handleDeleteDevice(response);
    });
  }

  handleDeleteDevice(response:DeviceDeletedResponse){
    this.peripheralDevices = this.peripheralDevices.filter(item => item.peripheralDeviceOid !== response.obj);
  }

}
