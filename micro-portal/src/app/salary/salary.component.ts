import { Component, OnInit, ViewChild } from '@angular/core';
import { SalarySheet, SalarySheetResponse, CalculateSalaryRequest, CommonResponse, HttpclientService } from '../service/httpclient.service';
import { NotificationService } from '../service/notification.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-salary',
  templateUrl: './salary.component.html',
  styleUrls: ['./salary.component.css']
})
export class SalaryComponent implements OnInit {

  month: string = "";
  year: string = "";
  totalCount: number; serial: number;
  salarySheetResponse:SalarySheetResponse;
  salarySheets:SalarySheet[] = [];
  loading: boolean = false;
  private destroyed$: Subject<boolean> = new Subject<boolean>();
  months: any = [
    {'key': '1', 'value': 'January'},
    {'key': '2', 'value': 'February'},
    {'key': '3', 'value': 'March'},
    {'key': '4', 'value': 'April'},
    {'key': '5', 'value': 'May'},
    {'key': '6', 'value': 'June'},
    {'key': '7', 'value': 'July'},
    {'key': '8', 'value': 'August'},
    {'key': '9', 'value': 'September'},
    {'key': '10', 'value': 'Octber'},
    {'key': '11', 'value': 'November'},
    {'key': '12', 'value': 'December'}
  ]


  years: any = [
    {'key': '2020', 'value': '2020'},
    {'key': '2021', 'value': '2021'},
    {'key': '2022', 'value': '2022'},
    {'key': '2023', 'value': '2023'},
    {'key': '2024', 'value': '2024'},
    {'key': '2025', 'value': '2025'},
    {'key': '2026', 'value': '2026'},
    {'key': '2027', 'value': '2027'},
    {'key': '2028', 'value': '2028'},
    {'key': '2029', 'value': '2029'},
    {'key': '2030', 'value': '2030'},
    {'key': '2031', 'value': '2031'},
    {'key': '2032', 'value': '2032'}
  ]

  displayedColumns: string[] = ['name', 'month', 'year', 'gradeNumber', 'gradeName', 'basic', 'houseRent', 'medicalAllowance', 'totalSalary'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private httpClientService:HttpclientService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
  }

  pageChanged(event){
    // this.loading = true;
 
     let pageIndex = event.pageIndex;
     let pageSize = event.pageSize;
 
     let previousIndex = event.previousPageIndex;
 
     let previousSize = pageSize * pageIndex;
 
     this.getNextData(previousSize, (pageIndex).toString(), pageSize.toString());
   }
 
   getNextData(currentSize, offset, limit){
     this.httpClientService.getSalarySheet(offset, limit, this.month, this.year).subscribe(
       response => {
         this.loading = false;
         this.salarySheets.length = currentSize;
         this.salarySheets.push(...response.data);
         this.salarySheets.length = response.count;
         this.dataSource = new MatTableDataSource<any>(this.salarySheets);
         this.dataSource._updateChangeSubscription();
         this.dataSource.paginator = this.paginator;
       }
     );
   }

  calculateSalary() {
    if(this.month == undefined || this.month == null || this.month == ""){
      this.notificationService.showError("Month can not be empty");
      return;
    }
    if(this.year == undefined || this.year == null || this.year == ""){
      this.notificationService.showError("Year can not be empty");
      return;
    }

    this.httpClientService.calculateSalary(new CalculateSalaryRequest(this.month, this.year)).subscribe(response =>{
      this.handleResponseSalaryCal(response);
    },
    error => this.handleErrorBasicSalaryCal(error.error.message)
    );
  }

  handleErrorBasicSalaryCal(errorMessage:String){
    this.notificationService.showError(errorMessage);
  }

  handleResponseSalaryCal(response:CommonResponse){
    this.notificationService.showSuccess(response.userMessage);
    
    this.httpClientService.getSalarySheet("0", "10", this.month, this.year).subscribe(
      response => this.handleResponse(response)
    );   
  }

  handleResponse(response:SalarySheetResponse){
    this.loading = false;
    this.salarySheets = response.data;
    this.salarySheets.length = response.count;
    this.dataSource = new MatTableDataSource<any>(this.salarySheets);
    this.dataSource.paginator = this.paginator;
  }

  downloadSalaryStatement() {
    if(this.month == undefined || this.month == null || this.month == ""){
      this.notificationService.showError("Month can not be empty");
      return;
    }
    if(this.year == undefined || this.year == null || this.year == ""){
      this.notificationService.showError("Year can not be empty");
      return;
    }

    this.loading = true;
    this.httpClientService.downloadSalarySheet(this.month, this.year).pipe(takeUntil(this.destroyed$),
      finalize(() => {
        this.loading = false;
        })).subscribe(file => {
      const unsafeImageUrl = URL.createObjectURL(file);
      window.open(unsafeImageUrl, '_blank');
    });
  }

}
