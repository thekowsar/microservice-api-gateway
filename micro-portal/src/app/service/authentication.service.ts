import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { HttpclientService, User, UserToken } from './httpclient.service';
import { NotificationService } from './notification.service';



@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private router: Router,
    private httpClient: HttpclientService,
    private notificationService: NotificationService
  ) { }

  
  authenticate(username:string, password:string){
    let user = new User(username, password);
    this.httpClient.authenticate(user).subscribe(response =>{
      this.handleResponse(response);
    },
      error => this.handleError(error.error.message)
    );
  }

  handleError(errorMessage){
    this.notificationService.showError(errorMessage);
  }

  handleResponse(response:UserToken){
    sessionStorage.setItem('token',response.token);
    this.router.navigate([''])
  }

  isUserLoggedIn() {
    let token = sessionStorage.getItem('token');
    return !(token === null);
  }

  logOut() {
    sessionStorage.removeItem('token');
  }
}
