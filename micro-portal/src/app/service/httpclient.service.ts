import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { APIConfig, GatewayApi } from '../constant/api-config';
import { Observable } from 'rxjs';

export class Gateway{
  constructor(
    public gatewayOid:string,
    public serialNumber:string,
    public gatewayName:string,
    public ipv4Address:string,
    public peripheralDevices: PeripheralDevice[],
  ){}
}

export class Grade{
  constructor(
    public gradeOid:string,
    public gradeNumber:string,
    public basic:string,
    public houseRentInPer:string,
    public medicalAllowanceInPer:string,
    public createdBy:string,
    public createdOn:string,
    public updatedBy:string,
    public updatedOn:string,
    public employees: Employee[],
  ){}
}

export class Employee {
  constructor(
    public employeeOid:string,
    public employeeId:String
  ){}
}

export class EmployeeInfo {
  constructor(
    public employeeOid:string,
    public employeeId:String,
    public name:string,
    public address:String,
    public mobileNo:string,
    public createdBy:String,
    public createdOn:string,
    public updatedBy:String,
    public updatedOn:string,
    public bankAccounts:EmployeeBankaccount[]
  ){}
}

export class EmployeeBankaccount {
  constructor(
    public bankAccountOid:string,
    public bankAccountNo:String,
    public accountName:string,
    public accountType:String,
    public bankName:string,
    public branchName:String,
    public currentBalance:string,
    public createdBy:String,
    public createdOn:string,
    public updatedBy:String,
    public updatedOn:String
  ){}
}

export class EmployeeListResponse{
  constructor(
    public data:EmployeeInfo[],
    public count:number,
    public userMessage:string,
  ){}
}

export class SaveEmployee {
  constructor(
    public name:string,
    public address:String,
    public mobileNo:string,
    public gradeOid:String,
    public accountName:string,
    public accountType:String,
    public bankAccountNo:string,
    public bankName:String,
    public branchName:String,
  ){}
}

export class BasicSalary{
  constructor(
    public basicSalary:string,
  ){}
}

export class CommonResponse{
  constructor(
    public obj:String,
    public userMessage:string
  ){}
}

export class CompanyBankAccountEntity{
  constructor(
    public bankAccountOid:String,
    public bankAccountNo:string,
    public accountName:String,
    public accountType:string,
    public bankName:String,
    public branchName:string,
    public currentBalance:String,
    public createdBy:string,
    public createdOn:String,
    public updatedBy:string,
    public updatedOn:String,
  ){}
}

export class CompanyAccountInfo{
  constructor(
    public companyBankAccountEntity:CompanyBankAccountEntity,
    public userMessage:string,
  ){}
}

export class Deposit{
  constructor(
    public amount:String,
    public bankAccountNo:string,
  ){}
}



export class PeripheralDevice {
  constructor(
    public peripheralDeviceOid:string,
    public uid:String,
    public vendor:string,
    public status:string,
    public gatewayOid:string,
    public createdOn:string
  ){}
}

export class GatewayCreate{
  constructor(
    public serialNumber:string,
    public gatewayName:string,
    public ipv4Address:string,
  ){}
}

export class DeviceCreate{
  constructor(
    public vendor:string,
    public status:string,
    public gatewayOid:string,
  ){}
}

export class GatewayResponse{
  constructor(
    public data:Gateway[],
    public count:number,
    public userMessage:string,
  ){}
}

export class GradeResponse{
  constructor(
    public data:Grade[],
    public count:number,
    public userMessage:string,
  ){}
}

export class GatewayByOidResponse{
  constructor(
    public obj:Gateway,
    public userMessage:string
  ){}
}

export class DeviceDeletedResponse{
  constructor(
    public obj:String,
    public userMessage:string
  ){}
}

export class User{
  constructor(
    public username:string,
    public password:string,
     ) {}
  
}

export class UserToken{
  constructor(
    public token:string,
     ) {}
  
}

export class CalculateSalaryRequest{
  constructor(
    public month:string,
    public year:string,
     ) {}
}



export class SalarySheet{
  constructor(
    public salarySheetOid:string,
    public month:string,
    public year:string,
    public gradeNumber:string,
    public gradeName:string,
    public basic:number,
    public houseRentInPer:number,
    public houseRent:number,
    public medicalAllowanceInPer:number,
    public medicalAllowance: number,
    public employeeOid:string,
    public name:string,
    public createdBy:string,
    public createdOn:string,
    public updatedBy:string,
    public updatedOn:string,
     ) {}
}

export class SalarySheetResponse{
  constructor(
    public data:SalarySheet[],
    public count:number,
    public userMessage:string,
  ){}
}

@Injectable({
  providedIn: 'root'
})
export class HttpclientService {

  constructor(
    private httpClient:HttpClient
  ) { }

  getGateways(offset:string, limit:string){
    let params = new HttpParams()
    .set('offset', offset)
    .set('limit', limit);
    return this.httpClient.get<GatewayResponse>(GatewayApi.GATEWAY_GET_ALL_URL, {params});
  }



  getGrades(offset:string, limit:string){
    let params = new HttpParams()
    .set('offset', offset)
    .set('limit', limit);
    return this.httpClient.get<GradeResponse>(GatewayApi.GRADE_GET_ALL_URL, {params});
  }

  authenticate(user:User){
    return this.httpClient.post<UserToken>(GatewayApi.USER_LOGIN_URL, user);
  }

  updateBasicSalary(basicSalary:BasicSalary){
    return this.httpClient.put<CommonResponse>(GatewayApi.GRADE_UPDATE_BASIC_URL, basicSalary);
  }

  getCompanyBankAccountInfo(){
    return this.httpClient.get<CompanyAccountInfo>(GatewayApi.COMPAN_BANK_ACCOUNT_GET_URL);
  }

  depositToCompanyAccount(deposit:Deposit){
    return this.httpClient.put<CommonResponse>(GatewayApi.COMPAN_BANK_ACCOUNT_DEPOSIT_PUT_URL, deposit);
  }

  saveEmployeeInfo(saveEmployee:SaveEmployee){
    return this.httpClient.post<CommonResponse>(GatewayApi.EMPLOYEE_INFO_SAVE_URL, saveEmployee);
  }

  calculateSalary(calculateSalaryRequest:CalculateSalaryRequest){
    return this.httpClient.post<CommonResponse>(GatewayApi.CALCULATE_SALARY_URL, calculateSalaryRequest);
  }
  
  getSalarySheet(offset:string, limit:string, month:string, year:string){
    let params = new HttpParams()
    .set('month', month)
    .set('year', year)
    .set('offset', offset)
    .set('limit', limit);
    return this.httpClient.get<SalarySheetResponse>(GatewayApi.SALARY_SHEET_GET_URL, {params});
  }

  getEmployeeInfo(offset:string, limit:string){
    let params = new HttpParams()
    .set('offset', offset)
    .set('limit', limit);
    return this.httpClient.get<EmployeeListResponse>(GatewayApi.EMPLOYEE_INFO_GET_URL, {params});
  }

  getAccountsRes(){
    return this.httpClient.get<GatewayByOidResponse>(GatewayApi.GAT_ACCOUNT_INFO);
  }

  downloadSalarySheet(month: string, year: string): Observable<Blob> {
    const getFileURI = `${APIConfig.BASE_API_URL}/api/hr/v1/salary/sheet/pdf?year=${year}&month=${month}`;
    return this.httpClient.get(getFileURI , {
        responseType : 'blob'
    });
}

  createGateway(gatewayCreate:GatewayCreate){
    return this.httpClient.post<Gateway>(GatewayApi.GATEWAY_SAVE_URL, gatewayCreate);
  }

  getGatewayByOid(oid:String){
    return this.httpClient.get<GatewayByOidResponse>(GatewayApi.GATEWAY_GET_BY_OID_URL + oid);
  }

  createDevice(deviceCreate:DeviceCreate){
    return this.httpClient.post<Gateway>(GatewayApi.DEVICE_SAVE_URL, deviceCreate);
  }

  deleteDevice(oid:String){
    return this.httpClient.delete<DeviceDeletedResponse>(GatewayApi.DEVICE_DELETE_URL + oid);
  }
}
